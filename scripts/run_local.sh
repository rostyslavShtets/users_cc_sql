#!/usr/bin/env bash

./scripts/build.sh
docker stop dnshost-1
docker rm dnshost-1
docker build -t dnshost .
docker run --name dnshost-1 -p 8080:80 -p 8053:53/udp -e LOG_FILE=/var/log/dnshost -v /Users/oleksiikhrypko/log:/var/log  -d dnshost