package main

import (
	// "users_crud_compose_sql/models"
	"users_crud_compose_sql/db"
	"users_crud_compose_sql/api"
	"users_crud_compose_sql/utils"
	"github.com/go-chi/chi"
	"net/http"
	"fmt"
	"log"
)
const port = ":80"
// const port = ":8086"


func main() {
	utils.Hello()
	fmt.Println("new")
	db.InitDB("root:root@tcp(db:3306)/people")
	// db.InitDB("root:root@tcp(localhost:3306)/people")
		
	r := chi.NewRouter()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(".root"))
	})
	r.Get("/people", api.GetPeople)
	r.Get("/people/{personID}", api.GetPerson)
	r.Post("/people", api.CreatePerson)
	r.Put("/people/{personID}", api.UpdatePerson)
	r.Delete("/people/{personID}", api.DeletePerson)

	fmt.Printf("Serv on port %v....\n", port)
	log.Fatal(http.ListenAndServe(port, r))
}

