package db

import (
	"users_crud_compose_sql/models"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
	"fmt"

)

var db *sql.DB

func InitDB(dataSourceName string) {
	var err error
	db, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		log.Fatal(err)
	}
	// defer db.Close()

	err = db.Ping()
	if err != nil {
		for i := 1; i < 5; i++ {
			fmt.Printf("We are trying to connect to db, attempt %v...\n", i)
			time.Sleep(1500 * time.Millisecond)
			db, _ = sql.Open("mysql", dataSourceName)
			err = db.Ping()
			if err == nil {
				fmt.Printf("Database is connected!")
				return
			}
		}
		log.Fatal(err)
	}
}

func GetAllPeopleFromDB() ([]*models.Person, error) {
	rows, err := db.Query("SELECT * FROM people")

	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	people := make([]*models.Person, 0)
	for rows.Next() {
		pl := new(models.Person)
		err := rows.Scan(&pl.UserID, &pl.FirstName, &pl.LastName, &pl.Age)
		if err != nil {
			log.Fatal(err)
		}
		people = append(people, pl)

		if err = rows.Err(); err != nil {
			log.Fatal(err)
		}
	}
	return people, err
}

func GetOnePersonFromDB(id string) (*models.Person, error) {
	// fmt.Println("in db GetOnePersonFromDB")
	row := db.QueryRow("SELECT * FROM people WHERE userID = ?", id)
	person := new(models.Person)
	err := row.Scan(&person.UserID, &person.FirstName, &person.LastName, &person.Age)
	if err == sql.ErrNoRows {
		return person, err
	} else if err != nil {
		return person, err
	}
	return person, err
}

func DeletePersonInDB(id string) (int64, error) {
	result, err := db.Exec("DELETE FROM people WHERE userID = ?", id)
	if err != nil {
		return 0, err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return rowsAffected, err
}

func CreatePersonInDB(person *models.Person) (int64, error) {
	result, err := db.Exec("INSERT INTO people (FirstName, LastName, Age) VALUES(?, ?, ?)",
		person.FirstName, person.LastName, person.Age)
	if err != nil {
		fmt.Println("error with EXEC")
		return 0, err
	}
	rowsAffected, err := result.RowsAffected()
	return rowsAffected, err
}

func UpdatePersonInDB(person *models.Person, id string) (int64, error){
	fmt.Println("in db")
	result, err := db.Exec("UPDATE people SET FirstName = ?, LastName = ?, age = ? WHERE userID = ?",
		person.FirstName, person.LastName, person.Age, id)
	if err != nil {
		fmt.Println("error with update")
		return 0, err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return rowsAffected, err
}


	// stmt, err := db.Prepare("CREATE TABLE people (userID int NOT NULL AUTO_INCREMENT PRIMARY KEY, FirstName varchar(40), LastName varchar(40), Age int(3));")
	// if err != nil {
	// 	fmt.Println(err.Error())
	// }
	// _, err = stmt.Exec()
	// if err != nil {
	// 	fmt.Println(err.Error())
	// } else {
	// 	fmt.Println("Person Table successfully migrated....")
	// }
