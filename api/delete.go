package api

import (
	"net/http"
	"github.com/go-chi/chi"
	"fmt"
	"users_crud_compose_sql/db"
)

func DeletePerson (w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "personID")
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}

	rowsAffected, err := db.DeletePersonInDB(id)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	fmt.Fprintf(w, "Person with ID = %v deleted successfully (%d row affected)\n", id, rowsAffected)
}
