package api

import (
	"users_crud_compose_sql/db"
	"net/http"
	"log"
	"encoding/json"
	"github.com/go-chi/chi"
	"database/sql"
)

func GetPeople(w http.ResponseWriter, r *http.Request) {
	people, err := db.GetAllPeopleFromDB()
	if err != nil {
		log.Fatal(err)
		return
	}
	json.NewEncoder(w).Encode(people)
}

func GetPerson(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "personID")

	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	
	person, err := db.GetOnePersonFromDB(id)

	if err == sql.ErrNoRows {
		http.NotFound(w, r)
		return
	} else if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	json.NewEncoder(w).Encode(person)
}