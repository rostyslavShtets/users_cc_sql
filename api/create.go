package api

import (
	"users_crud_compose_sql/models"
	"users_crud_compose_sql/db"
	"io/ioutil"
	"log"
	"encoding/json"
	"fmt"
	"net/http"
)

func CreatePerson(w http.ResponseWriter, r *http.Request) {
	fmt.Println("CreatePerson")
	body, err := ioutil.ReadAll(r.Body)
	var person models.Person
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &person)
	if err != nil {
		log.Println("handlers SaveID error:", err)
		http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
		return
	}
	fmt.Println(person)

	//check for empty values
	if person.LastName == "" || person.FirstName == "" || person.Age == 0 {
		fmt.Println("unvalid json")
		http.Error(w, http.StatusText(400), 400)
		return
	}
	
	rowsAffected, err := db.CreatePersonInDB(&person)

	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	fmt.Fprintf(w, "Person %v created successfully (%d row affected)\n", person.UserID, rowsAffected)
}